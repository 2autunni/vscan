<?php
/**
 * wp-cli vscan 
 * 
 * Checks if plugins and themes have security vulnerabilities. 
 * This commmand uses API from  WPScan Vulnerability Database  
 * This work is a fork from plugin plugin-security-scanner of Glen Scott
 * Compatible with wordpress 4.3.1
 * 
 * @version  0.1
 * @author Marcello 'chico' Cerruti (http://www.2autunni.eu)
 * @author Glen Scott
 * 
 *
 * @license http://opensource.org/licenses/GPL-2.0
 * @copyright 2015 Marcello Cerruti
 * 
 * @see https://wpvulndb.com/api
 * @see https://wordpress.org/plugins/plugin-security-scanner/
 * 
 */

/*  Copyright 2015  Marcello Cerruti 

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License, version 2, as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program; if not, write to the Free Software
        Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( !defined( 'WP_CLI' ) ) return;

class VulnScan extends WP_CLI_Command {


  /**
     * Checks if plugins and themes have security vulnerabilities. 
     * This commmand uses API from WPScan Vulnerability Database 
     * This work is a fork from 'plugin-security-scanner' of Glen Scott
     * 
     * 
     * ## FLAGS
     *  [--themes]
     * : Check also themes
     * 
     *  [--quiet]
     * : Print output only if discovers vulnerabilities
     * 
     * ## EXAMPLES
     * 
     *     wp vscan
     *     wp vscan --themes
     *     wp vscan --quiet
     *     wp vscan --themes --quiet
     *     
     *     
     *
     * @synopsis [--themes] [--quiet]
     */

  function __invoke( $args, $assoc_args ) {
    $vulnerability_count = 0;
    if ( isset ($assoc_args['themes'] )){
      $vulnerabilities = $this->scan_themes();
      if(is_wp_error($vulnerabilities)){
        WP_CLI::error($vulnerabilities->get_error_message());
      }
      foreach ( $vulnerabilities as $plugin_name => $plugin_vulnerabilities ) {
        foreach ( $plugin_vulnerabilities as $vuln ) {
          $body .= $vuln->title . "\n";
          if(count($vuln->references->url))
            foreach (($vuln->references->url) as $url )
            $body.="\t $url\n";
            $vulnerability_count++;
        }
      }
      if($vulnerability_count)
        $body = "Vulnerabilities found in themes: \n" .$body."\n";
    }
    $tv=$vulnerability_count;
    $vulnerabilities = $this->scan_plugins();
    if(is_wp_error($vulnerabilities)){
      WP_CLI::error($vulnerabilities->get_error_message());
    }
    foreach ( $vulnerabilities as $plugin_name => $plugin_vulnerabilities ) {
      foreach ( $plugin_vulnerabilities as $vuln ) {
        $pbody .= $vuln->title . "\n";
        if(count($vuln->references->url))
          foreach (($vuln->references->url) as $url )
          $pbody.="\t$url\n";
          $vulnerability_count++;
      }
    }

    if($vulnerability_count>$tv)
      $body .= "Vulnerabilities found in plugins: \n" .$pbody."\n";

    if ($vulnerability_count)
      WP_CLI::error($body);
    if (!isset ($assoc_args['quiet']))
      WP_CLI::success( "Nothing to report");
  }

  function scan_plugins() {
    $vulnerabilities = array();
    $request = new WP_Http;
    foreach ( get_plugins() as $name => $details ) {
      // get unique name
      if ( preg_match( '|(.+)/|', $name, $matches ) ) {
        $plugin_key = $matches[1];
        $result = $request->request( 'https://wpvulndb.com/api/v2/plugins/' . $plugin_key );
        if(is_wp_error($result)){
          return $result;
        }
        if ( $result['body'] ) {
          $plugin = json_decode( $result['body'] );
          if ( isset( $plugin->$plugin_key->vulnerabilities ) ) {
            foreach ( $plugin->$plugin_key->vulnerabilities as $vuln ) {
              if ( ! isset($vuln->fixed_in) ||
                  version_compare( $details['Version'], $vuln->fixed_in, '<' ) ) {
                $vulnerabilities[$name][] = $vuln;
              }
            }
          }
        }
      }
    }
    return $vulnerabilities;
  }

  function scan_themes() {
    $vulnerabilities = array();
    $request = new WP_Http;
    foreach ( wp_get_themes() as $name => $details ) {
      $result = $request->request( 'https://wpvulndb.com/api/v2/themes/' . $name );
      if(is_wp_error($result)){
        return $result;
      }
      if ( $result['body'] ) {
        $theme = json_decode( $result['body'] );
        if ( isset( $theme->$name->vulnerabilities ) ) {
          foreach ( $theme->$name->vulnerabilities as $vuln ) {
            if ( !isset($vuln->fixed_in) || version_compare( $details['Version'], $vuln->fixed_in, '<' ) ) {
              $vulnerabilities[$name][] = $vuln;
            }
          }

        }
      }
    }
    return $vulnerabilities;

  }
}

WP_CLI::add_command( 'vscan', 'VulnScan' );
