## Vscan 

This command checks if plugins and themes have security vulnerabilities using API from WPScan Vulnerability Database. 

This work is a fork from 'plugin-security-scanner' of Glen Scott

## USAGE

wp-cli vscan [--themes] [--quiet]

## SETUP

* Install wp-cli

* Install Vscan

```
cd ~/.wp-cli
mkdir commands
cd commands
git clone https://gitlab.com/2autunni/vscan.git
```
* Create or edit the ~/.wp-cli/config.yml file so that it looks like this:

```
require:
  - commands/vscan/vscan.php
```
* use Vscan

```
wp vscan
```
